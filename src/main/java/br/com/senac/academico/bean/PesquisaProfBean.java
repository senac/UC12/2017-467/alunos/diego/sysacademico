/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academico.bean;

import br.com.senac.academico.dao.AlunoDAO;
import br.com.senac.academico.dao.ProfessorDAO;
import br.com.senac.academico.model.Aluno;
import br.com.senac.academico.model.Professor;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author sala302b
 */
@Named(value = "pesquisaProfBean")
@Dependent
public class PesquisaProfBean extends Bean{

    /**
     * Creates a new instance of PesquisaProfBean
     */
    public PesquisaProfBean() {
    }
    
    private Professor professorSelecionado;
    private List<Professor> lista;
    private ProfessorDAO dao;

    private String codigo;
    private String nome;

    @PostConstruct
    public void init() {
        try {
            dao = new ProfessorDAO();
            professorSelecionado = new Professor();
            lista = dao.findAll();

        } catch (Exception ex) {
            ex.printStackTrace();
            this.addMessageErro("Falha ao carregar itens.");
        }

    }

    public void pesquisa() {
        try {
            this.lista = this.dao.findByFiltro(codigo, nome);

        }catch(Exception ex){
            ex.printStackTrace();
            
        }

    }
    
    public void salvar(){
        
        if(this.professorSelecionado.getId()== 0){
            
            this.dao.save(professorSelecionado);
            this.addMessageInfo("Salvo com sucesso!");
            
        }else{
            
            this.dao.update(professorSelecionado);
            this.addMessageInfo("Alterado com sucesso!");
            
        }
        
        this.pesquisa();
        
        
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor getProfessorSelecionado() {
        return professorSelecionado;
    }

    public void setAlunoSelecionado(Professor professorSelecionado) {
        this.professorSelecionado = professorSelecionado;
    }

    public List<Professor> getLista() {
        return lista;
    }

    public void setLista(List<Professor> lista) {
        this.lista = lista;
    }

    
}
