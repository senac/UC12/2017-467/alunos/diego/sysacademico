/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.academico.bean;

import javax.inject.Named;
import javax.enterprise.context.Dependent;

/**
 *
 * @author sala302b
 */
@Named(value = "cadastroAlunoBean")
@Dependent
public class CadastroAlunoBean extends Bean{

    /**
     * Creates a new instance of CadastroAlunoBean
     */
    public CadastroAlunoBean() {
    }

    private int codigo;
    private String nome;
    private String cpf;
    private String endereco;
    private String complemento;
    private int numero;
    private String cidade;
    private String uf;    

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
    
    
    
    
}
